FROM registry.access.redhat.com/ubi8:latest
WORKDIR /app
COPY hello.txt .
CMD ["sleep", "infinity"]
